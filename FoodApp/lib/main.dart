import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:foodapp/screen/app.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  // TODO: Add Firebase Initialization
  await Firebase.initializeApp();
  runApp(const App());
}

