import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:foodapp/DAO/food_dao.dart';
import 'package:provider/provider.dart';
import '../DAO/user_dao.dart';
import '../model/food.dart';
import 'login.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) :super(key: key);


  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<StatefulWidget> {

  @override
  Widget build(BuildContext context) {

    final userDao = Provider.of<UserDao>(context,listen: false);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Lab6 - Food Store'),
          actions: [
            IconButton(
              tooltip: 'Log out your account',
              onPressed: (){
                userDao.logout();
              },
              icon: const Icon(Icons.logout),
            ),
          ],
        ),
        body: SafeArea(child: FoodList(),),

      ),
      theme: ThemeData(
        primarySwatch: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class FoodList extends StatefulWidget {


  FoodList({Key? key}) : super(key: key);

  final foodDao = FoodDao();

  @override
  _FoodListState createState() => _FoodListState();

  Widget build(BuildContext context) {
    return Container();
  }
}

class _FoodListState extends State<FoodList> {
  final ScrollController _scrollController = ScrollController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _categoryController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _imageController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    //WidgetsBinding.instance!.addPostFrameCallback((_)=> _scrollToTop());
    final userDao = Provider.of<UserDao>(context,listen: false);
    final foodDao = Provider.of<FoodDao>(context,listen: false);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Column(
      children: [
        _getFoodList(foodDao),
        FloatingActionButton(
          onPressed: () async{

            final food = await addDialog();
            print("Food name");
            print(food?.name);
        },
          child: Icon(Icons.add),
        )
      ],
    );
  }

  Widget _getFoodList(FoodDao foodDao) {

    return Expanded(
      child: StreamBuilder<QuerySnapshot>(
        stream: foodDao.getFoodStream(),
        builder: (context,snapshot){
          if(!snapshot.hasData) {
            return const Center(
              child: LinearProgressIndicator(),
            );
          }
          return _buildList(context,snapshot.data!.docs);
        },
      )
    );
  }

  Widget _buildList(BuildContext context,List<DocumentSnapshot>? snapshot){
    return ListView(
      controller: _scrollController,
      physics: const BouncingScrollPhysics(),
      padding: const EdgeInsets.only(top: 20),
      children: snapshot!.map((data)=> _buildListItem(context,data)).toList(),
    );
  }

  Widget _buildListItem(BuildContext context,DocumentSnapshot snapshot){
    final food = Food.fromSnapshot(snapshot);
    return foodWidget(food);
  }

  Widget foodWidget(Food food){
    //static int count = 0;
    print("food name");
    print(food.name);
    return Card(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ListTile(
              leading:
              Image.network(food.image!),
              title: Text(

                    makeShort(food.name!),
                style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: Colors.black87),
              ),
              subtitle: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Price: " +
                        food.price.toString(),
                    style:
                    const TextStyle(color: Colors.black54),
                  ),
                  Row(
                    children: [
                      for( int i=0;i< food.rating!.round() && i <= 5;i++)
                        const Icon(Icons.star,color: Colors.yellow,)
                    ],
                  )
                ],
              ),
              trailing: const Icon(Icons.create),
            ),
          ],
        ),
      ),
    );
  }


  // Non-widget Functions
  String makeShort(String title) {
    if (title.length > 20) {
      return title.substring(0, 20) + "...";
    }
    return title;
  }

  void search(String query) {
    print("Query here");
    print(query);
  }

  void _scrollToTop(){
    if(_scrollController.hasClients){
      _scrollController.jumpTo(_scrollController.position.minScrollExtent);
    }
  }

  Future<Food?> addDialog() => showDialog<Food>(
      context: context,
    builder: (context) => AlertDialog(
      title: Text('Add a new Food'),
      content: Column(
        children: [
          TextFormField(
            controller: _nameController,
            decoration: const InputDecoration(
              hintText: 'Enter the name of food',
              labelText: 'Name',
            ),
            autofocus: true,
            autocorrect: false
          ),
          TextFormField(
              controller: _categoryController,
              decoration: const InputDecoration(
                hintText: "Enter the category",
                labelText: 'Category',
              ),
              autofocus: true,
              autocorrect: false
          ),
          TextFormField(
              controller: _descriptionController,
              decoration: const InputDecoration(
                hintText: "Enter the food's description",
                labelText: 'Description',
              ),
              autofocus: true,
              autocorrect: false
          ),
          TextFormField(
              controller: _priceController,
              decoration: const InputDecoration(
                hintText: "Enter the price",
                labelText: 'Price',
              ),
              autofocus: true,
              autocorrect: false,
              keyboardType: const TextInputType.numberWithOptions(signed: false,decimal: true),
          ),
          TextFormField(
              controller: _imageController,
              decoration: const InputDecoration(
                hintText: "Enter the URL image of food",
                labelText: 'Image',
              ),
              autofocus: true,
              autocorrect: false
          ),
        ],
      ),
      actions: [
        TextButton(onPressed: submit, child: const Text('Create'))
      ],
    )

  );

  void submit(){
    Food food =  Food();
    food.name = _nameController.text;
    food.price = double.parse(_priceController.text);
    food.description = _descriptionController.text;
    food.category = _categoryController.text;
    food.image = _imageController.text;
    food.rating = 0;

    Navigator.of(context).pop(food);

    _nameController.clear();
    _priceController.clear();
    _descriptionController.clear();
    _categoryController.clear();
    _imageController.clear();

  }


}

























