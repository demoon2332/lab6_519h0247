import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:foodapp/DAO/user_dao.dart';
import 'package:provider/provider.dart';
import '../validator/login_validator.dart';
import 'home.dart';

class LoginScreen extends StatefulWidget {

  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginScreenState();
  }
}

class LoginScreenState extends State<StatefulWidget> with LoginValidator {
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passController = TextEditingController();

  @override
  void dispose() {
    emailController.dispose();
    passController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          title: const Center(
            child: Text('Food Store Login'),
          ),
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(32),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  Row(
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      Expanded(child: emailField())
                    ],
                  ),
                  Row(
                    children: [
                      const SizedBox(height: 20),
                      Expanded(
                        child: passwordField(),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      const SizedBox(height: 20),
                      Expanded(
                        child: loginButton(context),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      const SizedBox(height: 20),
                      Expanded(
                        child: registerButton(context),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ) //body
        );
  }

  Widget passwordField() {
    return TextFormField(
      controller: passController,
      obscureText: true,
      decoration: const InputDecoration(
          border: UnderlineInputBorder(),
          icon: Icon(Icons.password),
          labelText: 'Password',
          hintText: 'Enter password'),
      autofocus: false,
      textCapitalization: TextCapitalization.none,
      keyboardType: TextInputType.visiblePassword,
      autocorrect: false,
      validator: validatePass,
    );
  }

  Widget emailField() {
    return TextFormField(
      controller: emailController,
      decoration: const InputDecoration(
        icon: Icon(Icons.person),
        border: UnderlineInputBorder(),
        labelText: 'Email address',
        hintText: 'Enter email',
      ),
      autocorrect: false,
      autofocus: false,
      textCapitalization: TextCapitalization.none,
      keyboardType: TextInputType.emailAddress,
      validator: validateEmail,
    );
  }

  Widget loginButton(BuildContext context) {
    final userDao = Provider.of<UserDao>(context, listen: false);
    double width = MediaQuery.of(context).size.width;
    return ElevatedButton(
        onPressed: (){
          print("EmaiL: "+emailController.text);
          print("Pass: "+passController.text);
          userDao.login(emailController.text,passController.text);
        },
        child: const Padding(
          padding: EdgeInsets.all(6),
          child: Text('Login'),
        ));
  }

  Widget registerButton(BuildContext context) {
    final userDao = Provider.of<UserDao>(context, listen: false);
    double width = MediaQuery.of(context).size.width;
    return ElevatedButton(
        onPressed: (){
          userDao.signup(emailController.text,passController.text);
        },
        child: const Padding(
          padding: EdgeInsets.all(6),
          child: Text('Register an account'),
        ));
  }
}
