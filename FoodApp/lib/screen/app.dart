import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../DAO/food_dao.dart';
import 'package:provider/provider.dart';
import '../DAO/user_dao.dart';
import 'home.dart';
import 'login.dart';


class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {

    return MultiProvider(providers: [
      Provider<FoodDao>(
        lazy: true,
        create: (_)=>FoodDao(),
      ),
      ChangeNotifierProvider<UserDao>(
        lazy: false,
        create: (_)=> UserDao()
      )
    ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Food Store',
        theme: ThemeData(primaryColor: const Color(0xFF3D814A)),
        home: Consumer<UserDao>(
          builder: (context,userDao,child){
            if(userDao.isLoggedIn()){
              return const HomeScreen();
            }
            else{
              return const LoginScreen();
            }
          },
        )
      ),
    );
  }
}