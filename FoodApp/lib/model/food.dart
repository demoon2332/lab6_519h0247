import 'package:cloud_firestore/cloud_firestore.dart';

class Food {
  String? name;
  double? price;
  String? description;
  String? category;
  String? image;
  double? rating;
  DocumentReference? reference;

  Food(
      {
        this.name,
        this.price,
        this.description,
        this.category,
        this.image,
        this.rating,
        this.reference});


  factory Food.fromJson(Map<dynamic, dynamic> json) => Food(
    name: json['name'] as String?,
    price: double.parse(json['price'].toString()),
    description: json['description'] as String?,
    category: json['category'] as String?,
    image: json['image'] as String?,
    rating: double.parse(json['rating'].toString()),
  );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['name'] = this.name;
    data['price'] = this.price;
    data['description'] = this.description;
    data['category'] = this.category;
    data['image'] = this.image;
    data['rating'] = this.rating;
    return data;
  }

  factory Food.fromSnapshot(DocumentSnapshot snapshot){
    final food = Food.fromJson(snapshot.data() as Map<String, dynamic>);
    food.reference = snapshot.reference;
    return food;
  }

}
