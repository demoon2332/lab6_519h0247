import 'package:cloud_firestore/cloud_firestore.dart';
import '../model/food.dart';

class FoodDao {
  final CollectionReference foodList = FirebaseFirestore.instance.collection('food');

  void addFood(Food food){
    foodList.add(food.toJson());
  }

  Stream<QuerySnapshot> getFoodStream(){
    return foodList.snapshots();
  }
}